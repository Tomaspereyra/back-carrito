package com.danaide.examenTecnico.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.danaide.examenTecnico.app.service.IProductoService;
import com.danaide.examenTecnico.app.utils.RestResponse;

@RestController
@RequestMapping("/productos")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductosController {
	@Autowired
	private IProductoService productoService;
	
	@Autowired
	private RestResponse response;
	
	@GetMapping("/{categoria}")
	public RestResponse consultaProductoPorCategoria(@PathVariable String categoria) {
		return null;
	}
	@GetMapping("/destacados")
	@ResponseStatus(code = HttpStatus.OK)
	public RestResponse consultarProductosDestacados() {
		try {
			response = this.productoService.traerProductosDestacados();
			
		}catch(Exception e) {
			response.setCod(502);
			response.setMensaje("Ocurrio un error desconocido al intentar consultar los productos destacados");
			response.setData(null);
			
			
		}
		
		return response;
	}

}

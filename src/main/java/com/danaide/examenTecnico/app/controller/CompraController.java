package com.danaide.examenTecnico.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.danaide.examenTecnico.app.service.IOrdenService;
import com.danaide.examenTecnico.app.utils.RestResponse;

@RestController
@RequestMapping("/compra")
public class CompraController {
	
	@Autowired
	private RestResponse response;
	
	@Autowired
	private IOrdenService compraService;
	
	@PostMapping("/finalizar")
	@ResponseStatus(code = HttpStatus.CREATED)
	public RestResponse efectuarCompra() {
		
		return null;
	}

}

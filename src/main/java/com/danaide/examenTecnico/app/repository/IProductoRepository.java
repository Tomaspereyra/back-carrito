package com.danaide.examenTecnico.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.danaide.examenTecnico.app.model.Producto;
@Repository
public interface IProductoRepository extends JpaRepository<Producto,Integer>{
	
	public List<Producto> findBydestacado(Boolean destacado);

}

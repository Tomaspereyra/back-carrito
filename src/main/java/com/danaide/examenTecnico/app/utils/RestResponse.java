package com.danaide.examenTecnico.app.utils;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class RestResponse {
	private Integer cod;
	private String mensaje;
	private Map<String,Object> data;
	public Integer getCod() {
		return cod;
	}
	public void setCod(Integer cod) {
		this.cod = cod;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
	

}

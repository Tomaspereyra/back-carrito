package com.danaide.examenTecnico.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="tipo_clientes")
public class TipoCliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String nombre;
	@Temporal(TemporalType.DATE)
	private Date desde;
	@Temporal(TemporalType.DATE)
	private Date hasta;
	@OneToOne(mappedBy = "tipoCliente")
	private Cliente cliente;
	

}

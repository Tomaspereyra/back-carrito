package com.danaide.examenTecnico.app.service;

import com.danaide.examenTecnico.app.utils.RestResponse;

public interface IClienteService {
	public RestResponse consultaClientesVip();
	
	public RestResponse consultaClientesVipPorMesAno();
	
	public RestResponse consultaClientesExVipPorMesAno();
	
	public RestResponse actualizarCliente();
	
	
}

package com.danaide.examenTecnico.app.service;

import com.danaide.examenTecnico.app.model.Orden;
import com.danaide.examenTecnico.app.utils.RestResponse;

public interface IOrdenService {
	
	public RestResponse confirmarCompra(Orden orden);
	
	

}

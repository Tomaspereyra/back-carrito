package com.danaide.examenTecnico.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danaide.examenTecnico.app.model.Orden;
import com.danaide.examenTecnico.app.repository.IOrdenRepository;
import com.danaide.examenTecnico.app.service.IOrdenService;
import com.danaide.examenTecnico.app.utils.RestResponse;
@Service
public class OrdenService implements IOrdenService{
	
	@Autowired
	private IOrdenRepository ordenRepository;
	

	@Override
	public RestResponse confirmarCompra(Orden orden) {
		RestResponse response = new RestResponse();

		try {
			this.ordenRepository.save(orden);
			response.setCod(200);
			response.setMensaje("Compra guardada con exito.");
			response.setData(null);
			
		}catch(Exception e)
		{	
			response.setCod(502);
			response.setMensaje("Ocurrio un error al intentar guardar la compra.");
			response.setData(null);
			
		}
		return response;
	}

}

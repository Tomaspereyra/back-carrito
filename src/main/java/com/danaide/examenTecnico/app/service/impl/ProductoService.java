package com.danaide.examenTecnico.app.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danaide.examenTecnico.app.model.Producto;
import com.danaide.examenTecnico.app.repository.IProductoRepository;
import com.danaide.examenTecnico.app.service.IProductoService;
import com.danaide.examenTecnico.app.utils.RestResponse;



@Service
public class ProductoService implements IProductoService{
	@Autowired
	private IProductoRepository productoRepository;
	@Autowired
	private RestResponse response;
	@Override
	public RestResponse traerProductosPorCategoria(String categoria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RestResponse traerProductosDestacados() throws Exception {
		
			List<Producto> productos = this.productoRepository.findBydestacado(Boolean.TRUE);
			if(productos != null) {
				Map<String,Object> productosMap = new HashMap<>();
				productosMap.put("productos", productos);
				response.setCod(200);
				response.setMensaje("Se encontraron productos destacados con exito");
				response.setData(productosMap);
			}else {
				response.setCod(404);
				response.setMensaje("No se encontraron productos destacados");
				response.setData(null);

			}
			
			
	
		return response;
	}

}

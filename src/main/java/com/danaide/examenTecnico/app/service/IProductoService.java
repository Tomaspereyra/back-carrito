package com.danaide.examenTecnico.app.service;

import com.danaide.examenTecnico.app.utils.RestResponse;

public interface IProductoService {
	
	public RestResponse traerProductosPorCategoria(String categoria) throws Exception;
	
	public RestResponse traerProductosDestacados() throws Exception;

}
